# Tic-Tac-Toe
- This package includes vanilla JS implementation of classic Tic-Tac-Toe game.
- Opponent can be whether AI or person.
- AI opponent uses `Minimax` algorithm to find best move from state tree.
- Real opponent can compete with user via `Websocket` protocol.
